
@available(macOS 10.15, *)
extension CollectionDifference : CustomStringConvertible where ChangeElement == String.Element  {
    public var description: String {
        "Diff: [ " +
        self.map {
            switch $0 {
            case .insert(offset: let offset, element: let element, _):
                return "+\(element)@\(offset)".replacingOccurrences(of: "\n", with: "\\n")
            case .remove(offset: let offset, element: let element, _):
                return "-\(element)@\(offset)".replacingOccurrences(of: "\n", with: "\\n")
            }
        }.joined(separator: ", ") + " ]"
    }
}
