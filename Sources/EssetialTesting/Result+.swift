import XCTest
import Essentials

public extension Result {
    func verify(_ topic: String? = nil) -> Self {
        onSuccess {
            topic?.print(success: $0)
        }.onFailure {
            (topic ?? "verify").print(failure: $0)
            XCTAssert(false)
        }
    }
    
    @discardableResult
    func shouldSucceed(_ topic: String? = nil) -> Success? {
        onSuccess {
            if isMultiline($0) {
                topic?.appending(" [Multiline]").print(success: "\n\($0)")
            } else {
                topic?.print(success: $0)
            }
            
        }.onFailure {
            (topic ?? "shouldSucceed").print(failure: $0)
            XCTAssert(false)
        }
        return maybeSuccess
    }
        
    @discardableResult
    func shouldFail(_ topic: String? = nil) -> Success? {
        onSuccess {
            (topic ?? "shouldFail").print(success: $0)
            XCTAssert(false)
        }.onFailure {
            topic?.print(failure: $0, asci: false)
        }
        return maybeSuccess
    }
    
    private func isMultiline(_ obj: Success) -> Bool {
        if let str = obj as? String {
            return str.isMultiline
        }
        return false
    }
    
    @available(macOS 10.15, *)
    private func diff(_ left: Success, _ right : Success, prefix: String = "") -> String {
        guard let left = left as? String else { return "" }
        guard let right = right as? String else { return "" }
        
        return prefix + String(describing: left.difference(from: right))
    }

    @available(macOS 10.15, *)
    @discardableResult
    func assertEqual(to: Success, _ topic: String? = nil) -> Success? where Success: Equatable {
        onSuccess {
            if to == $0 {
                if isMultiline($0) {
                    topic?.appending(" [Multiline]").print(success: "\n\($0) \n   ==\n \(to)")
                } else {
                    topic?.print(success: "\($0) == \(to)")
                }
            } else {
                if isMultiline($0) {
                    let diff = diff($0, to, prefix: "\n")
                    (topic ?? "Assert Equal [Multiline]").print(failure: WTF("\n\($0)\n   !=\n\(to)" + diff))
                } else {
                    let diff = diff($0, to, prefix: " | ")
                    (topic ?? "Assert Equal").print(failure: WTF("\($0) != \(to)" + diff))
                }
                
            }
            XCTAssert(to == $0)
        }.onFailure {
            topic?.print(failure: $0)
            XCTAssert(false)
        }
        return maybeSuccess
    }

    @available(macOS 10.15, *)
    @discardableResult
    func assertNotEqual(to: Success, _ topic: String? = nil) -> Success? where Success: Equatable {
        onSuccess {
            if to != $0 {
                if isMultiline(to) {
                    let diff = diff($0, to, prefix: "\n")
                    topic?.appending(" [Multiline]").print(success: "\n\($0)\n   !=\n\(to)" + diff)
                } else {
                    let diff = diff($0, to, prefix: " | ")
                    topic?.print(success: "\($0) != \(to)" + diff)
                }
            } else {
                if isMultiline(to) {
                    (topic ?? "Assert NOT Equal").appending(" [Multiline]").print(failure: WTF("\($0) == \(to)"))
                } else {
                    (topic ?? "Assert NOT Equal").print(failure: WTF("\($0) == \(to)"))
                }
            }
            XCTAssert(to != $0)
        }.onFailure {
            topic?.print(failure: $0)
            XCTAssert(false)
        }
        return maybeSuccess
    }
    
    @discardableResult
    func assertBlock(_ topic: String? = nil, block: (Success) -> Bool) -> Success? {
        onSuccess {
            if block($0) {
                if isMultiline($0) {
                    topic?.appending(" [Multiline]").print(success: "\n\($0)")
                } else {
                    topic?.print(success: $0)
                }
            } else {
                topic?.print(failure: WTF("assertion failed for \($0)"))
                XCTAssert(false, topic ?? "")
            }
            
            
        }.onFailure {
            topic?.print(failure: $0)
            XCTAssert(false)
        }
        return maybeSuccess
    }
}


private extension Result {
    @discardableResult
    func onSuccess(_ block: (Success)->Void) -> Self {
        switch self {
        case .success(let success): block(success)
        default: break
        }
        return self
    }
    
    @discardableResult
    func onFailure(_ block: (Error)->Void) -> Self {
        switch self {
        case .failure(let error): block(error)
        default: break
        }
        return self
    }
}

