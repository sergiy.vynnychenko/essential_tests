
import Foundation

extension String {
    var isMultiline : Bool {
        if let first = self.range(of: "\n") {
            let from = self.index(after: first.upperBound)
            if let _ = self.range(of: "\n", range: from..<self.endIndex) {
                return true
            }
        }
        return false
    }
}

extension String {
    func print<T>(success: T) {
        Swift.print("+-----------------------------------------------)")
        Swift.print("+ SUCCEEDED: \(self)")
        Swift.print("+ WITH:      \(success)")
        Swift.print("+-----------------------------------------------)")
    }

    func print(failure: Error, asci: Bool = true) {
        let nsError = failure as NSError
        Swift.print("+-----------------------------------------------)")
        Swift.print("+ FAILED:    \(self)")
        Swift.print("+ WITH:      \(failure.localizedDescription)")
        if let reason = nsError.localizedFailureReason {
        Swift.print("+ REASON:    \(reason)")
        }
        Swift.print("+ CODE:      \(nsError.code)")
        Swift.print("+-----------------------------------------------)")
        if asci {
            Swift.print(bug)
        }
    }
}

let bug = #"""
    \ /
    oVo
\___XXX___/
 __XXXXX__
/__XXXXX__\
/   XXX   \
     V
"""#
