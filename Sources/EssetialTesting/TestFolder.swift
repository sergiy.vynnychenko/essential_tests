//
//  File.swift
//  
//
//  Created by loki on 11.02.2022.
//

import Foundation
import Essentials

public struct TestFolder {
    public let url : URL
    
    public init(url: URL) {
        self.url = url
        _ = url.makeSureDirExist()
    }
    
    public func sub(folder: String) -> TestFolder {
        TestFolder(url: self.url.appendingPathComponent(folder))
    }
    
    public func cleared() -> R<TestFolder> {
        url.rm() | { url.makeSureDirExist() } | { _ in self }
    }
}
